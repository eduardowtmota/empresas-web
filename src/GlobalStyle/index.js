import { createGlobalStyle } from "styled-components";

const GlobalStyle = createGlobalStyle`
    *{
        box-sizing: border-box;
        padding: 0;
        margin: 0;
        font-family: 'Roboto', sans-serif;
    }

    body{
        background-color: #eeecdb;
    }

    input, button{

        &:focus{
            outline:none;
        }
    }

    button{
        cursor: pointer;
    }

    ul{
        list-style: none;
    }

    a{
        text-decoration: none;
    }
`;

export default GlobalStyle;
