import { Switch, Route } from "react-router-dom";

// PAGES
import Login from "../pages/Login";
import Home from "../pages/Home";
import DetailsCompany from "../pages/DetailsCompany";

const Routes = () => {
  return (
    <Switch>
      <Route exact path="/" component={Login} />
      <Route exact path="/home" component={Home} />
      <Route exact path="/home/:id" component={DetailsCompany} />
    </Switch>
  );
};

export default Routes;
