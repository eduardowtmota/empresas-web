// STYLE
import GlobalStyle from "./GlobalStyle";

// ROUTES
import Routes from "./Routes";

const App = () => {
  return (
    <>
      <GlobalStyle />
      <Routes />
    </>
  );
};

export default App;
