import { FindCompanyType } from "./actionsType";

export const FindCompany = (Company) => ({
  type: FindCompanyType,
  Company,
});
