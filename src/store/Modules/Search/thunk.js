import axios from "axios";

import { FindCompany } from "./actions";

export const FindCompanyThunk = (SearchValue) => (dispatch, getState) => {
  const { company } = getState();
  const baseURL = "https://empresas.ioasys.com.br/api/v1/enterprises?name=";

  const info = JSON.parse(window.localStorage.getItem("info"));

  const headers = {
    ...info,
    "Content-Type": "application/json",
  };

  if (company !== SearchValue && SearchValue !== "") {
    axios
      .get(`${baseURL + SearchValue}`, { headers: headers })
      .then((res) => dispatch(FindCompany(res.data.enterprises)));
  } else {
    dispatch(FindCompany(""));
  }
};
