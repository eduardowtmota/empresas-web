import { FindCompanyType } from "./actionsType";

const SearchReducers = (state = "", action) => {
  const { Company, type } = action;
  switch (type) {
    case FindCompanyType:
      return Company;
    default:
      return state;
  }
};

export default SearchReducers;
