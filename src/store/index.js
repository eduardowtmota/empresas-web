import { createStore, combineReducers, applyMiddleware } from "redux";

import thunk from "redux-thunk";

// REDUCERS
import SearchReducers from "./Modules/Search/reducers";

const reducers = combineReducers({
  company: SearchReducers,
});

const store = createStore(reducers, applyMiddleware(thunk));

export default store;
