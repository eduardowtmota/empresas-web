import { useState } from "react";

// IMAGES
import Brand from "../../assets/img/brand.png";

// COMPONENTS
import LoginForm from "../../components/LoginForm";
import LoginHeader from "../../components/LoginHeader";
import Loading from "../../components/Loading";

// STYLE
import { Container } from "./styles";

const Login = () => {
  const [loading, setLoading] = useState(false);

  return (
    <>
      <Container>
        <img src={Brand} alt="Ioasys" title="Ioasys" />

        <LoginHeader />

        <LoginForm setLoading={setLoading} />
      </Container>
      {loading && <Loading setLoading={setLoading} />}
    </>
  );
};

export default Login;
