import styled from "styled-components";

export const Container = styled.div`
  min-height: 100vh;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  width: 100%;

  @media (max-width: 400px) {
    width: 90%;
    margin: 0 auto;
  }

  > img {
    width: 250px;
    height: auto;
  }
`;
