import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import axios from "axios";

// COMPONENT
import Header from "../../components/Header";
import DetailsContent from "../../components/DetailsContent";

const DetailsCompany = () => {
  const [user, setUser] = useState({});
  const { id } = useParams();

  useEffect(() => {
    const baseURL = `https://empresas.ioasys.com.br/api/v1/enterprises/${id}`;

    const info = JSON.parse(window.localStorage.getItem("info"));

    const headers = {
      ...info,
      "Content-Type": "application/json",
    };

    axios
      .get(baseURL, { headers: headers })
      .then((res) => setUser(res.data.enterprise));
  }, [id]);

  return (
    Object.keys(user).length !== 0 && (
      <>
        <Header title={user.enterprise_name} />
        <DetailsContent user={user} />
      </>
    )
  );
};

export default DetailsCompany;
