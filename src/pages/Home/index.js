// COMPONENTS
import Header from "../../components/Header";
import Results from "../../components/Results";

const Home = () => {
  return (
    <>
      <Header />
      <Results />
    </>
  );
};

export default Home;
