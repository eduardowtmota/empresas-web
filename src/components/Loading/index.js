// IMAGES
import LoadingIcon from "../../assets/img/loading.gif";

// STYLES
import { Container } from "./styles";

const Loading = ({ setLoading }) => {
  setTimeout(() => {
    setLoading(false);
  }, 1000);

  return (
    <Container>
      <img src={LoadingIcon} alt="Loading" />
    </Container>
  );
};

export default Loading;
