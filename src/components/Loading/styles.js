import styled from "styled-components";

export const Container = styled.div`
  min-height: 100vh;
  width: 100%;
  position: absolute;
  top: 0;
  left: 0;
  height: 100%;
  backdrop-filter: blur(1px);
  background-color: rgba(255, 255, 255, 0.6);
  display: flex;
  justify-content: center;
  align-items: center;
`;
