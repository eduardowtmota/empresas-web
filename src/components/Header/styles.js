import styled, { keyframes } from "styled-components";

const fadeIn = keyframes`
  0% {
    opacity: 0;
  }
  100% {
    opacity: 1;
  }
`;

export const Container = styled.div`
  height: 90px;
  background-color: #ee4c77;
  position: relative;
  padding: 0 5%;

  > img {
    height: 60px;
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    animation: 1s ${fadeIn} ease-out;

    @media (max-width: 768px) {
      display: none;
    }
  }
`;

export const Box = styled.div`
  display: flex;
  height: 100%;
  align-items: center;

  svg {
    width: 30px;
    height: 30px;
    margin-right: 20px;
    fill: #fff;
  }

  button {
    background-color: transparent;
    border: none;
  }

  h2 {
    color: #fff;
    font-size: 1.7rem;
  }
`;
