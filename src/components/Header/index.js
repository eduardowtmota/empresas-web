import { useState } from "react";
import { useHistory, useParams } from "react-router-dom";

// COMPONENTS
import SearchBar from "../SearchBar";

// IMAGES
import BrandLight from "../../assets/img/brandLight.png";

// STYLE
import { Container, Box } from "./styles";

// ICONS
import { BiLeftArrowAlt } from "react-icons/bi";

const Header = ({ title }) => {
  const [open, setOpen] = useState(false);
  const history = useHistory();
  const { id } = useParams();

  return (
    <Container>
      {id === undefined ? (
        <>
          {!open && <img src={BrandLight} alt="Ioasys" title="Ioasys" />}

          <SearchBar setOpen={setOpen} open={open} />
        </>
      ) : (
        <Box>
          <button onClick={() => history.push("/home")}>
            <BiLeftArrowAlt />
          </button>
          <h2>{title}</h2>
        </Box>
      )}
    </Container>
  );
};

export default Header;
