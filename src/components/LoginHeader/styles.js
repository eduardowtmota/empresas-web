import styled from "styled-components";

export const Container = styled.div`
  text-align: center;
  margin: 45px 0;

  h1 {
    width: 50%;
    margin: 0 auto 30px;
    text-transform: uppercase;
    font-size: 1.6rem;
    font-weight: bold;
    color: #383743;

    @media (max-width: 400px) {
      width: 100%;
      font-size: 1.4rem;
    }
  }

  p {
    width: 60%;
    margin: 0 auto;
    color: #383743;

    @media (max-width: 400px) {
      width: 100%;
    }
  }
`;
