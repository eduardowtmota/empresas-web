import { useState, useCallback } from "react";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import axios from "axios";
import { useHistory } from "react-router-dom";

// ICONS
import Lock from "../../assets/img/lock.png";
import Mail from "../../assets/img/mail.png";
import { AiFillEye, AiFillEyeInvisible } from "react-icons/ai";

// STYLE
import { Form, FormItem, SendForm } from "./styles";

const LoginForm = ({ setLoading }) => {
  const [passwordVisible, setPasswordVisible] = useState(false);
  const [formErrors, setFormErrors] = useState(false);
  const history = useHistory();

  const schema = yup.object().shape({
    email: yup.string().required(),
    password: yup.string().min(6, "Campo com no mínimo 6 caracteres"),
  });

  const { register, handleSubmit, errors } = useForm({
    resolver: yupResolver(schema),
  });

  const changeVisiblePassword = useCallback(
    (e) => {
      e.preventDefault();
      setLoading(true);
      setPasswordVisible(!passwordVisible);
    },
    [passwordVisible, setLoading]
  );

  const handleForm = (data) => {
    const baseURL = "https://empresas.ioasys.com.br/api/v1/users/auth/sign_in";

    axios
      .post(baseURL, data, { headers: { "Content-Type": "application/json" } })
      .then(({ headers }) => {
        setFormErrors(false);
        const info = {
          "access-token": headers["access-token"],
          client: headers["client"],
          uid: headers["uid"],
        };
        window.localStorage.setItem("info", JSON.stringify(info));
        history.push("/home");
      })
      .catch(() => {
        setFormErrors(true);
      });
  };

  return (
    <Form onSubmit={handleSubmit(handleForm)}>
      <FormItem>
        <img src={Mail} alt="Login Mail" title="Login Mail" />
        <input type="text" placeholder="E-mail" name="email" ref={register} />
        {Object.keys(errors).length > 0 && <span>!</span>}
      </FormItem>

      <FormItem>
        <img src={Lock} alt="Login Lock" title="Login Lock" />
        <input
          type={passwordVisible ? "text" : "password"}
          placeholder="Senha"
          name="password"
          ref={register}
        />
        {Object.keys(errors).length > 0 ? (
          <span>!</span>
        ) : (
          <button onClick={(e) => changeVisiblePassword(e)}>
            {passwordVisible ? <AiFillEye /> : <AiFillEyeInvisible />}
          </button>
        )}
      </FormItem>

      {(Object.keys(errors).length > 0 || formErrors) && (
        <span>Credenciais informadas são inválidas, tente novamente.</span>
      )}

      <SendForm type="submit">Entrar</SendForm>
    </Form>
  );
};

export default LoginForm;
