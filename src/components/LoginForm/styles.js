import styled from "styled-components";

export const Form = styled.form`
  width: 350px;
  display: flex;
  flex-direction: column;
  align-items: center;

  > span {
    font-size: 0.85rem;
    color: #ff0f44;
    margin: -18px 0 20px;
    transition: all 0.3s;
  }
`;

export const FormItem = styled.div`
  position: relative;
  height: 50px;
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  margin-bottom: 30px;

  &:nth-of-type(2) {
    margin-bottom: 40px;
  }

  img {
    width: 30px;
    height: 30px;
    position: absolute;
    left: 0;
    top: 50%;
    transform: translateY(-50%);
  }

  input {
    width: 100%;
    background-color: transparent;
    border: none;
    border-bottom: 1px solid #383743;
    padding-left: 50px;
    height: 100%;
    font-size: 1rem;
  }

  > button {
    position: absolute;
    right: 10px;
    background-color: transparent;
    border: none;
  }

  > button > svg {
    width: 20px;
    height: 20px;
  }

  span {
    position: absolute;
    right: 10px;
    height: 30px;
    width: 30px;
    background-color: #ff0f44;
    border-radius: 50%;
    display: flex;
    align-items: center;
    justify-content: center;
    color: #fff;
  }
`;

export const SendForm = styled.button`
  background-color: #57bbbc;
  border: none;
  width: 100%;
  height: 50px;
  text-transform: uppercase;
  color: #fff;
  font-size: 1rem;
  border-radius: 5px;
  transition: all 0.3s;

  &:hover {
    background-color: #4eacad;
  }
`;
