import { Link } from "react-router-dom";

// STYLES
import { Card, CardContent, ContentLeft, ContentRight } from "./styles";

const CompanyCard = ({ result }) => {
  const baseURL = "https://empresas.ioasys.com.br/";

  return (
    <Card>
      <Link to={`home/${result.id}`}>
        <CardContent>
          <ContentLeft>
            <img
              src={baseURL + result.photo}
              alt={result.enterprise_name}
              title={result.enterprise_name}
            />
          </ContentLeft>
          <ContentRight>
            <h3>{result.enterprise_name}</h3>
            <p>{result.enterprise_type.enterprise_type_name}</p>
            <span>{result.country}</span>
          </ContentRight>
        </CardContent>
      </Link>
    </Card>
  );
};

export default CompanyCard;
