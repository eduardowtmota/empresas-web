import styled from "styled-components";

export const Card = styled.li`
  width: 25%;
  padding: 0 15px;
  margin-top: 30px;

  @media (max-width: 1440px) {
    width: 33.3333%;
  }

  @media (max-width: 992px) {
    width: 50%;
  }

  @media (max-width: 768px) {
    width: 100%;
  }
`;

export const CardContent = styled.div`
  background-color: #fff;
  border-radius: 4px;
  padding: 20px;
  height: 160px;
  display: flex;
  justify-content: space-between;
  transition: all 0.3s;

  @media (max-width: 1440px) {
    height: 145px;
  }

  @media (max-width: 768px) {
    height: 180px;
  }

  @media (max-width: 450px) {
    height: 140px;
  }

  h3 {
    color: #1a0e49;
    font-size: 1.2rem;
    transition: all 0.3s;
  }

  p {
    color: #8d8c8c;
    font-size: 0.9rem;
    transition: all 0.3s;
  }

  span {
    color: #8d8c8c;
    font-size: 0.8rem;
    transition: all 0.3s;
  }

  &:hover {
    background-color: #ee4c77;

    h3,
    p,
    span {
      color: #fff;
    }
  }
`;

export const ContentLeft = styled.div`
  width: 40%;

  img {
    width: 100%;
    height: 100%;
    object-fit: cover;
  }
`;

export const ContentRight = styled.div`
  width: 55%;
  display: flex;
  flex-direction: column;
  justify-content: space-around;
`;
