import styled from "styled-components";

export const Container = styled.div`
  min-height: 90vh;
  display: flex;
  align-items: center;
  justify-content: center;

  h1 {
    font-weight: normal;
    color: #383743;
    font-size: 1.8rem;
  }

  h2 {
    font-weight: normal;
    color: #b5b4b4;
    font-size: 1.8rem;
  }
`;

export const CardList = styled.ul`
  display: flex;
  flex-flow: row wrap;
  padding: 20px 0 100px;
`;
