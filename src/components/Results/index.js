import { useSelector } from "react-redux";

// COMPONENTS
import CompanyCard from "../CompanyCard";

// STYLE
import { Container, CardList } from "./styles";

const Results = () => {
  const company = useSelector(({ company }) => company);

  if (company === "") {
    return (
      <Container>
        <h1>Clique na busca para iniciar.</h1>
      </Container>
    );
  }

  if (company.length === 0) {
    return (
      <Container>
        <h2>Nenhuma empresa foi encontrada para a busca realizada.</h2>
      </Container>
    );
  }

  return (
    <CardList>
      {company.map((result, index) => (
        <CompanyCard result={result} key={index} />
      ))}
    </CardList>
  );
};

export default Results;
