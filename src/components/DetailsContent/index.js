// STYLE
import { Container, Card } from "./styles";

const DetailsContent = ({ user }) => {
  const baseURL = "https://empresas.ioasys.com.br/";

  return (
    <Container>
      <Card>
        <img
          src={baseURL + user.photo}
          alt={user.enterprise_name}
          title={user.enterprise_name}
        />
        <p>{user.description}</p>
      </Card>
    </Container>
  );
};

export default DetailsContent;
