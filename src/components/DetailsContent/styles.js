import styled from "styled-components";

export const Container = styled.div`
  width: 70%;
  margin: 50px auto 100px;

  @media (max-width: 1200px) {
    width: 90%;
  }
`;

export const Card = styled.div`
  background-color: #fff;
  border-radius: 4px;
  padding: 80px;

  @media (max-width: 992px) {
    padding: 40px;
  }

  @media (max-width: 400px) {
    padding: 30px;
  }

  img {
    width: 100%;
    height: auto;
    object-fit: cover;
  }

  p {
    color: #8d8c8c;
    margin-top: 50px;
    font-size: 1.2rem;

    @media (max-width: 768px) {
      font-size: 1rem;
      margin-top: 30px;
    }
  }
`;
