import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";

// ACTION
import { FindCompanyThunk } from "../../store/Modules/Search/thunk";

// ICONS
import { AiOutlineSearch } from "react-icons/ai";
import { GrClose } from "react-icons/gr";

// STYLE
import { Container, Box } from "./styles";

const SearchBar = ({ setOpen, open }) => {
  const dispatch = useDispatch();
  const [company, setCompany] = useState("");

  const seachCompany = (e) => {
    setCompany(e.target.value);
  };

  useEffect(() => {
    dispatch(FindCompanyThunk(company));
  }, [dispatch, company]);

  return (
    <Container>
      {open ? (
        <Box>
          <AiOutlineSearch />
          <input
            type="text"
            placeholder="Pesquisar"
            value={company}
            onChange={seachCompany}
          />
          <button onClick={() => setOpen(false)}>
            <GrClose />
          </button>
        </Box>
      ) : (
        <button onClick={() => setOpen(true)}>
          <AiOutlineSearch />
        </button>
      )}
    </Container>
  );
};

export default SearchBar;
