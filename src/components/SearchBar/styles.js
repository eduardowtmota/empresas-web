import styled, { keyframes } from "styled-components";

const fadeIn = keyframes`
  0% {
    opacity: 0;
  }
  100% {
    opacity: 1;
  }
`;

export const Container = styled.div`
  height: 100%;
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: flex-end;

  button {
    background-color: transparent;
    border: none;
  }

  svg {
    width: 20px;
    height: 20px;
    fill: #fff;
    animation: 1s ${fadeIn} ease-out;
  }

  svg path {
    stroke: #fff;
  }
`;

export const Box = styled.div`
  position: relative;
  width: 400px;
  height: 65%;
  animation: 1s ${fadeIn} ease-out;

  @media (max-width: 768px) {
    width: 100%;
  }

  > svg {
    position: absolute;
    top: 50%;
    left: 0;
    transform: translateY(-50%);
  }

  > button svg {
    position: absolute;
    top: 50%;
    right: 0;
    transform: translateY(-50%);
  }

  input {
    width: 100%;
    background-color: transparent;
    border: none;
    border-bottom: 1px solid #fff;
    height: 100%;
    padding-left: 35px;
    font-size: 1.2rem;
    color: #fff;

    &::-webkit-input-placeholder {
      color: #fff;
    }
  }
`;
